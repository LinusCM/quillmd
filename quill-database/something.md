Okay, so I'm setting up these endpoints:

## `GET /documents` -> gives back list of documents

returns:

```json
[
  {
    "doc_id": 1234,
    "title": "some title",
    "created_at": "datetime",
    "last_updated": "datetime",
    "username": "username of document owner"
  }
]
```

## `POST /documents` -> allows the user to upload a new document

returns:

```json
{
  "doc_id": 1234,
  "title": "some title",
  "body": "The document body",
  "created_at": "datetime",
  "last_updated": "datetime",
  "username": "username of document owner"
}
```

receives:

```json
{
  "title": "some title",
  "body": "The document body"
}
```

## `GET /documents/:document_id` -> returns the specified document object

returns:

```json
{
  "doc_id": 1234,
  "title": "some title",
  "body": "The document body",
  "created_at": "datetime",
  "last_updated": "datetime",
  "username": "username of document owner"
}
```

## `PUT /documents/:document_id` -> modify existing document

returns `204` if document has been modified succesfully.

receives:

```json
{
  "title": "some title",
  "body": "The document body"
}
```

## `DELETE /documents/:document_id` -> deletes existing document

returns `204` if the document has been deleted succesfully.

## `GET /documents/:document_id/access` -> gets list of users that have access to the document (other than the owner)

returns:

```json
[
  {
    "username": "the username of a user with access",
    "doc_role": "is one of two values 'viewer', or 'writer'"
  }
]
```

## `POST /documents/:document_id/access/:username/:doc_role` -> allows you to add a user to the access list

It will give the specified username the specified doc_role (which needs to be either `viewer`, or `writer`.

returns `204` if user is added succesfully, otherwise it returns `400`.

## `DELETE /documents/:document_id/access/:username` -> allows you to remove a user from the access list.

returns `204` if user is sucesfully deleted from the access list.
