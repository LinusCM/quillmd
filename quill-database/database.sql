-- Create the GLOBAL_ROLES table
CREATE TABLE global_roles (
    role_name VARCHAR(255) PRIMARY KEY
);

-- Create the USERS table
CREATE TABLE users (
    username VARCHAR(255) PRIMARY KEY,
    password VARCHAR(255) NOT NULL,
    salt VARCHAR(255) NOT NULL,
    global_role VARCHAR(255) NOT NULL,
    FOREIGN KEY (global_role) REFERENCES global_roles (role_name)
);

-- Create the DOCS table
CREATE TABLE docs (
    doc_id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    title VARCHAR(255) NOT NULL,
    body TEXT NOT NULL,
    created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    last_updated TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    username VARCHAR(255) NOT NULL,
    FOREIGN KEY (username) REFERENCES users (username) ON DELETE CASCADE
);

-- Create the DOC_ROLES table
CREATE TABLE doc_roles (
    role_name VARCHAR(255) PRIMARY KEY
);

-- Create the USER_ACCESS table
CREATE TABLE user_access (
    access_id INTEGER PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    username VARCHAR(255) NOT NULL,
    doc_id INT NOT NULL,
    doc_role VARCHAR(255) NOT NULL,
    FOREIGN KEY (username) REFERENCES users (username) ON DELETE CASCADE,
    FOREIGN KEY (doc_id) REFERENCES docs (doc_id) ON DELETE CASCADE,
    FOREIGN KEY (doc_role) REFERENCES doc_roles (role_name)
);

-- Populate the global_roles table with initial data
INSERT INTO global_roles (role_name) VALUES ('user'), ('admin');

-- Populate the doc_roles table with initial data
INSERT INTO doc_roles (role_name) VALUES ('viewer'), ('writer');
