{mypkgs ? import <nixpkgs> {}}:
with mypkgs; let
  inherit (lib) optional optionals;
in
  mkShell {
    buildInputs =
      [
        postgresql
      ]
      ++ optional stdenv.isLinux inotify-tools # For file_system on Linux.
      ++ optionals stdenv.isDarwin (with darwin.apple_sdk.frameworks; [
        # For file_system on macOS.
        CoreFoundation
        CoreServices
      ]);

    # Put the PostgreSQL databases in the project directory.
    shellHook = ''
      export PGDATA="$(pwd)/db"
      export PGHOST="$(pwd)"
      export PGPORT="5432"
      dbInitialized=0

      if [[ ! -d $PGDATA ]]; then
        echo "db does not exist, initializing"
        initdb -D $PGDATA --no-locale --encoding=UTF8 >> /dev/null
        cat >> "$PGDATA/postgresql.conf" <<-EOF
        listen_addresses = 'localhost'
        port = $PGPORT
        unix_socket_directories = '$PGHOST'
      EOF
        echo "CREATE USER postgres SUPERUSER;" | postgres --single -E postgres
        dbInitialized=1
      fi

      pg_ctl -D ./db -l logfile start

      if [[ $dbInitialized -eq 1 ]]; then
        echo "Initializing database with database.sql"
        psql -U postgres -d postgres -a -f database.sql
      fi

      echo "Database has been started"

      trap "pg_ctl -D ./db stop" EXIT
    '';
  }
