import { Routes } from "@angular/router";
import { EditorComponent } from "./components/editor/editor.component";
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";

export const routes: Routes = [
  { path: "signin", component: LoginComponent },
  { path: "signup", component: RegisterComponent },
  { path: "editor", component: EditorComponent },
  { path: "", redirectTo: "/signin", pathMatch: "full" },
];
