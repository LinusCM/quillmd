export interface DocAccess {
  username: string;
  docRole: DocRole;
}

export enum DocRole {
  Read = "read",
  Write = "write",
}
