export interface Doc {
  docId: number;
  titel: string;
  body: string;
  createdAt: string;
  lastUpdated: string;
  username: string;
}
