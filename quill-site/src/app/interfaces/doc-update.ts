export interface DocUpdate {
  title: string;
  body: string;
}
