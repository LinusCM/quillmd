import { Injectable } from "@angular/core";
import { Doc } from "../interfaces/doc";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, BehaviorSubject, Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class DocService {
  // We do not want anyone to update our list, so we keep our subject/entries private.
  private docs: Array<Doc> = [];
  private docsSubject$: Subject<Doc[]> = new BehaviorSubject<Doc[]>(this.docs);

  // Can only be subscribed to, as the subject is now an observable.
  docs$: Observable<Doc[]> = this.docsSubject$.asObservable();

  constructor(private http: HttpClient) {}
}
