import { Component, OnInit } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { marked } from "marked";
import { Subject } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { MatIconModule } from "@angular/material/icon";
import { MatButtonModule } from "@angular/material/button";

@Component({
  selector: "app-editor",
  standalone: true,
  imports: [FormsModule, MatIconModule, MatButtonModule],
  templateUrl: "./editor.component.html",
  styleUrl: "./editor.component.scss",
})
export class EditorComponent implements OnInit {
  markdownText: string = "";
  renderedMarkdown: string | Promise<string> = "";
  private inputChanged: Subject<void> = new Subject<void>();

  constructor() {}

  ngOnInit(): void {
    this.inputChanged.pipe(debounceTime(500)).subscribe(() => {
      this.convertToHtml();
    });
  }

  onInputChanged() {
    this.inputChanged.next();
  }

  convertToHtml() {
    this.renderedMarkdown = marked(this.markdownText);
  }

  save() {
    // TODO: add save call to API
  }
}
