import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import { MatFormField, MatLabel } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { LoginService } from "../../services/login.service";
import { Credentials } from "../../interfaces/credentials";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";

@Component({
  selector: "app-register",
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatFormField,
    MatLabel,
    MatInputModule,
    MatButtonModule,
    MatCardModule,
  ],
  templateUrl: "./register.component.html",
  styleUrl: "./register.component.scss",
})
export class RegisterComponent {
  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private snackBar: MatSnackBar,
    private router: Router,
  ) {
    this.registerForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  onRegister(): void {
    if (this.registerForm.valid) {
      const newLogin = this.registerForm.value as Credentials;

      this.loginService.register(newLogin).subscribe(
        (response) => {
          localStorage.setItem("accessToken", response.token);

          this.router.navigate(["/editor"]);
        },
        (error) => {
          this.snackBar.open(
            "Registration failed. Please check your credentials.",
            "Dismiss",
            {
              duration: 3000,
              horizontalPosition: "center",
              verticalPosition: "bottom",
            },
          );
        },
      );
    }
  }
}
