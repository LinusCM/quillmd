# Database diagram

```mermaid
erDiagram
    GLOBAL_ROLES {
        string role_name PK "Primary Key"
    }
    USERS {
        string username PK "Primary Key"
        string password "Not Null"
        string salt "Not Null"
        string global_role "Not Null, FK"
    }
    DOCS {
        int doc_id PK "Primary Key, Generated"
        string title "Not Null"
        text body "Not Null"
        timestamp created_at "Not Null"
        timestamp last_updated "Not Null"
        string username "Not Null, FK"
    }
    DOC_ROLES {
        string role_name PK "Primary Key"
    }
    USER_ACCESS {
        int access_id PK "Primary Key, Generated"
        string username "Not Null, FK"
        int doc_id "Not Null, FK"
        string doc_role "Not Null, FK"
    }
    GLOBAL_ROLES ||--o{ USERS : "FK_global_role"
    USERS ||--o{ DOCS : "FK_username_docs"
    USERS ||--o{ USER_ACCESS : "FK_username_user_access"
    DOCS ||--o{ USER_ACCESS : "FK_doc_id"
    DOC_ROLES ||--o{ USER_ACCESS : "FK_doc_role"
```
