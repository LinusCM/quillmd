use serde::{Deserialize, Serialize};

/// This is the structure of our claims for the JWT token.
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Claims {
    pub sub: String,
    pub aud: String,
}

impl Claims {
    pub fn new(sub: impl Into<String>, aud: impl Into<String>) -> Self {
        Self {
            sub: sub.into(),
            aud: aud.into(),
        }
    }
}
