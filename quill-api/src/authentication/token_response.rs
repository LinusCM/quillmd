use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TokenResponse {
    access_token: String,
}

impl TokenResponse {
    pub fn new(access_token: String) -> Self {
        Self { access_token }
    }
}
