use super::hasher;
use super::user::{GlobalRole, User};
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, PgPool};

#[derive(Serialize, Deserialize)]
pub struct Credentials {
    username: String,
    password: String,
}

impl Credentials {
    /// Returns an empty Ok if successful login.
    /// Returns an Err with the username of the account the attempt with on otherwise.
    pub async fn check_login(self, pool: PgPool) -> Result<String, ()> {
        let Ok(user) = query_as!(
            User,
            "SELECT * FROM users WHERE username = $1",
            self.username
        )
        .fetch_one(&pool)
        .await
        else {
            return Err(());
        };

        match hasher::verify_password(&self.password, &user.salt, &user.password) {
            true => Ok(self.username),
            false => Err(()),
        }
    }

    pub async fn add_user(self, pool: PgPool) -> Result<String, ()> {
        if let Ok(_) = query!("SELECT * FROM users WHERE username = $1", self.username)
            .fetch_one(&pool)
            .await
        {
            return Err(());
        };

        let salt = hasher::generate_salt();
        let hash = hasher::hash_password(&self.password, &salt);

        match query!(
            "INSERT INTO users (username,password,salt,global_role) VALUES ($1,$2,$3,$4)",
            self.username,
            hash,
            salt,
            &Into::<String>::into(GlobalRole::User),
        )
        .execute(&pool)
        .await
        {
            Ok(_) => Ok(self.username),
            Err(_) => Err(()),
        }
    }
}
