use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct User {
    pub username: String,
    pub password: String,
    pub salt: String,
    pub global_role: GlobalRole,
}

#[derive(Serialize, Deserialize)]
pub enum GlobalRole {
    User,
    Admin,
}

impl Into<String> for GlobalRole {
    fn into(self) -> String {
        match self {
            GlobalRole::User => "user",
            GlobalRole::Admin => "admin",
        }
        .into()
    }
}

impl From<String> for GlobalRole {
    fn from(s: String) -> Self {
        match s.as_ref() {
            "user" => GlobalRole::User,
            "admin" => GlobalRole::Admin,
            _ => panic!("Invalid global role: {s}"),
        }
    }
}
