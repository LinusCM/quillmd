use base64::{engine::general_purpose, Engine};
use rand::Rng;
use ring::pbkdf2;
use std::num::NonZeroU32;

const ITERATIONS: NonZeroU32 = unsafe { NonZeroU32::new_unchecked(100_000) }; // Number of iterations for PBKDF2
const SALT_LEN: usize = 16; // 16 bytes salt
const KEY_LEN: usize = 32; // 32 bytes key

pub fn generate_salt() -> String {
    // the thread_rng().gen() method does not use the kernel to generate random numbers,
    // but it does get its seed from the system kernel,
    // which I think is safe enough, as we're getting a new seed from the kernel every time this function is run.
    let salt: [u8; SALT_LEN] = rand::thread_rng().gen();
    general_purpose::STANDARD.encode(&salt)
}

pub fn hash_password(password: &str, salt: &str) -> String {
    let salt_bytes = general_purpose::STANDARD.decode(salt).unwrap();

    let mut pbkdf2_hash = [0u8; KEY_LEN];

    pbkdf2::derive(
        pbkdf2::PBKDF2_HMAC_SHA256,
        ITERATIONS,
        &salt_bytes,
        password.as_bytes(),
        &mut pbkdf2_hash,
    );
    general_purpose::STANDARD.encode(&pbkdf2_hash)
}

pub fn verify_password(password: &str, salt: &str, hash: &str) -> bool {
    let salt_bytes = general_purpose::STANDARD.decode(salt).unwrap();

    let pbkdf2_hash = general_purpose::STANDARD.decode(hash).unwrap();

    pbkdf2::verify(
        pbkdf2::PBKDF2_HMAC_SHA256,
        ITERATIONS,
        &salt_bytes,
        password.as_bytes(),
        &pbkdf2_hash,
    )
    .is_ok()
}
