use axum::{extract::State, http::StatusCode, Json};
use sqlx::PgPool;

use crate::{authentication::claims::Claims, claims_to_token};

use super::{credentials::Credentials, token_response::TokenResponse};

/// We check if the login is correct, then generate and return a JWT token if true.
pub async fn login(
    State(pool): State<PgPool>,
    Json(payload): Json<Credentials>,
) -> Result<Json<TokenResponse>, StatusCode> {
    match payload.check_login(pool).await {
        Ok(username) => Ok(Json(claims_to_token(Claims::new(username, "user")))),
        Err(_) => Err(StatusCode::UNAUTHORIZED),
    }
}

pub async fn register(
    State(pool): State<PgPool>,
    Json(payload): Json<Credentials>,
) -> Result<Json<TokenResponse>, StatusCode> {
    match payload.add_user(pool).await {
        Ok(username) => Ok(Json(claims_to_token(Claims::new(username, "user")))),
        Err(_) => Err(StatusCode::UNAUTHORIZED),
    }
}
