use std::collections::HashSet;

use super::{claims::Claims, token_response::TokenResponse};
use axum::{
    extract::Request,
    http::{HeaderMap, StatusCode},
    middleware::Next,
    response::Response,
};
use dotenv_codegen::*;
use jsonwebtoken::{
    decode, encode, Algorithm, DecodingKey, EncodingKey, Header, TokenData, Validation,
};
use once_cell::sync::Lazy;

const TOKEN_SECRET: &[u8] = dotenv!("JWT_SECRET").as_bytes();
const AUDIENCE: &[&str] = &["user", "admin"];
const ALGORITHM: Algorithm = Algorithm::HS256;

// These constant variables are initialized lazily.
// Meaning that the first time they're accessed is when they will be constructed.
// After which they will be held in memory for future use.
const HEADER: Lazy<Header> = Lazy::new(|| Header::new(ALGORITHM));
const VALIDATION: Lazy<Validation> = Lazy::new(|| {
    let mut validator = Validation::new(ALGORITHM);
    validator.set_audience(AUDIENCE);
    validator.validate_exp = false;
    validator.required_spec_claims = HashSet::from(["sub".into(), "aud".into()]);
    validator
});

pub async fn check_token(
    headers: HeaderMap,
    mut request: Request,
    next: Next,
) -> Result<Response, StatusCode> {
    match get_token(headers) {
        Some(token) => {
            let Ok(token) = decode_token(&token) else {
                return Err(StatusCode::UNAUTHORIZED);
            };

            request.extensions_mut().insert(token.claims);
            Ok(next.run(request).await)
        }
        _ => Err(StatusCode::UNAUTHORIZED),
    }
}

/// We strip out the Bearer prefix, and extract the token.
pub fn get_token(headers: HeaderMap) -> Option<String> {
    headers.get("Authorization").and_then(|header_value| {
        header_value
            .to_str()
            .ok()
            .and_then(|header_str| header_str.strip_prefix("Bearer ").map(|token| token.into()))
    })
}

fn decode_token(token: &str) -> Result<TokenData<Claims>, jsonwebtoken::errors::Error> {
    decode::<Claims>(token, &DecodingKey::from_secret(TOKEN_SECRET), &VALIDATION)
}

/// When people login, this is where we convert the claims they need to a token response.
pub fn claims_to_token(claims: Claims) -> TokenResponse {
    TokenResponse::new(encode(&HEADER, &claims, &EncodingKey::from_secret(TOKEN_SECRET)).unwrap())
}
