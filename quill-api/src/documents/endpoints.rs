pub mod document_id_access;
pub mod documents;
pub mod documents_id;

pub use self::{document_id_access::*, documents::*, documents_id::*};

use axum::http::StatusCode;
use serde::{Deserialize, Serialize};
use sqlx::{query_as, PgPool};

use crate::{
    claims::Claims,
    document::{Document, DocumentRole, UserAccess},
};

#[derive(Serialize, Deserialize)]
pub struct DocumentArgument {
    pub title: String,
    pub body: String,
}

pub async fn check_document_access(
    id: i32,
    claims: Claims,
    pool: &PgPool,
) -> Result<(Document, DocumentRole), StatusCode> {
    let Ok(document) = query_as!(Document, "SELECT * FROM docs WHERE doc_id = $1", id)
        .fetch_one(pool)
        .await
    else {
        return Err(StatusCode::NOT_FOUND);
    };

    if document.username == claims.aud {
        return Ok((document, DocumentRole::Writer));
    }

    match query_as!(
        UserAccess,
        "SELECT * FROM user_access WHERE doc_id = $1 AND username = $2",
        id,
        claims.aud
    )
    .fetch_one(pool)
    .await
    {
        Ok(user_access) => Ok((document, user_access.doc_role)),
        Err(_) => Err(StatusCode::FORBIDDEN),
    }
}
