use crate::{claims::Claims, document::Document, DocumentArgument};

use axum::{extract::State, http::StatusCode, response::IntoResponse, Extension, Json};
use sqlx::{query_as, PgPool};

pub async fn get_documents(
    Extension(claims): Extension<Claims>,
    State(pool): State<PgPool>,
) -> Json<Vec<Document>> {
    let mut owned_documents: Vec<Document> = query_as!(
        Document,
        "SELECT * FROM docs WHERE username = $1",
        claims.aud
    )
    .fetch_all(&pool)
    .await
    .unwrap_or(Vec::new());

    let accessed_documents: Vec<Document> = query_as!(
        Document,
        "SELECT d.doc_id, d.title, d.body, d.created_at, d.last_updated, d.username \
        FROM docs d JOIN user_access ua ON d.doc_id = ua.doc_id \
        WHERE ua.username = $1",
        claims.aud
    )
    .fetch_all(&pool)
    .await
    .unwrap_or(Vec::new());

    owned_documents.extend(accessed_documents);

    Json(owned_documents)
}

pub async fn post_document(
    Extension(claims): Extension<Claims>,
    State(pool): State<PgPool>,
    Json(payload): Json<DocumentArgument>,
) -> Result<impl IntoResponse, impl IntoResponse> {
    match query_as!(
        Document,
        "INSERT INTO docs (title, body, created_at, last_updated, username) \
        VALUES ($1, $2, NOW(), NOW(), $3) \
        RETURNING *",
        payload.title,
        payload.body,
        claims.aud,
    )
    .fetch_one(&pool)
    .await
    {
        Ok(document) => Ok(Json(document)),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}
