use axum::{
    extract::{Path, State},
    http::StatusCode,
    Extension, Json,
};
use serde::{Deserialize, Serialize};
use sqlx::{query, query_as, PgPool};

use crate::{check_document_access, claims::Claims, document::DocumentRole};

#[derive(Serialize, Deserialize)]
pub struct UserAccessResponse {
    pub username: String,
    pub doc_role: DocumentRole,
}

pub async fn get_user_access(
    Path(document_id): Path<i32>,
    Extension(claims): Extension<Claims>,
    State(pool): State<PgPool>,
) -> Result<Json<Vec<UserAccessResponse>>, StatusCode> {
    let access = check_document_access(document_id, claims, &pool).await;
    if let Err(status_code) = access {
        return Err(status_code);
    };

    match query_as!(
        UserAccessResponse,
        "SELECT ua.username, ua.doc_role \
        FROM user_access ua \
        JOIN docs d ON ua.doc_id = d.doc_id \
        WHERE ua.doc_id = $1 \
        AND ua.username != d.username",
        document_id
    )
    .fetch_all(&pool)
    .await
    {
        Ok(user_access) => Ok(Json(user_access)),
        Err(_) => Err(StatusCode::INTERNAL_SERVER_ERROR),
    }
}

pub async fn post_user_access(
    Path((document_id, username, role)): Path<(i32, String, DocumentRole)>,
    Extension(claims): Extension<Claims>,
    State(pool): State<PgPool>,
) -> StatusCode {
    if let DocumentRole::Owner = role {
        return StatusCode::BAD_REQUEST;
    };

    let access = check_document_access(document_id, claims, &pool).await;
    if let Err(status_code) = access {
        return status_code;
    };

    match query!(
        "INSERT INTO user_access (username, doc_id, doc_role) VALUES ($1, $2, $3)",
        username,
        document_id,
        &Into::<String>::into(role),
    )
    .execute(&pool)
    .await
    {
        Ok(_) => StatusCode::NO_CONTENT,
        Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
    }
}

pub async fn delete_user_access(
    Path((document_id, username)): Path<(i32, String)>,
    Extension(claims): Extension<Claims>,
    State(pool): State<PgPool>,
) -> StatusCode {
    let access = check_document_access(document_id, claims, &pool).await;
    if let Err(status_code) = access {
        return status_code;
    };

    match query!(
        "DELETE FROM user_access WHERE doc_id = $1 AND username = $2",
        document_id,
        username
    )
    .execute(&pool)
    .await
    {
        Ok(_) => StatusCode::NO_CONTENT,
        Err(_) => StatusCode::NOT_FOUND,
    }
}
