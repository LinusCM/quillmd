use axum::{
    extract::{Path, State},
    http::StatusCode,
    Extension, Json,
};
use sqlx::{query, PgPool};

use crate::{
    check_document_access,
    claims::Claims,
    document::{Document, DocumentRole},
    DocumentArgument,
};

pub async fn get_document(
    Path(document_id): Path<i32>,
    Extension(claims): Extension<Claims>,
    State(pool): State<PgPool>,
) -> Result<Json<Document>, StatusCode> {
    Ok(Json(
        check_document_access(document_id, claims, &pool).await?.0,
    ))
}

pub async fn put_document(
    Path(document_id): Path<i32>,
    Extension(claims): Extension<Claims>,
    State(pool): State<PgPool>,
    Json(payload): Json<DocumentArgument>,
) -> StatusCode {
    match check_document_access(document_id, claims, &pool).await {
        Ok((_, access)) => {
            if let DocumentRole::Viewer = access {
                return StatusCode::FORBIDDEN;
            }

            match query!(
                "UPDATE docs SET title = $2, body = $3, last_updated = NOW() WHERE doc_id = $1",
                document_id,
                payload.title,
                payload.body
            )
            .execute(&pool)
            .await
            {
                Ok(_) => StatusCode::NO_CONTENT,
                Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
            }
        }
        Err(error_code) => error_code,
    }
}

pub async fn delete_document(
    Path(document_id): Path<i32>,
    Extension(claims): Extension<Claims>,
    State(pool): State<PgPool>,
) -> StatusCode {
    match check_document_access(document_id, claims, &pool).await {
        Ok((_, access)) => {
            if let DocumentRole::Viewer = access {
                return StatusCode::FORBIDDEN;
            }

            match query!("DELETE FROM docs WHERE doc_id = $1", document_id,)
                .execute(&pool)
                .await
            {
                Ok(_) => StatusCode::NO_CONTENT,
                Err(_) => StatusCode::INTERNAL_SERVER_ERROR,
            }
        }
        Err(error_code) => error_code,
    }
}
