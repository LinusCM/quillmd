use chrono::NaiveDateTime;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Document {
    pub doc_id: i32,
    pub title: String,
    pub body: String,
    pub created_at: NaiveDateTime,
    pub last_updated: NaiveDateTime,
    pub username: String,
}

#[derive(Serialize, Deserialize)]
pub struct UserAccess {
    pub access_id: i32,
    pub username: String,
    pub doc_id: i32,
    pub doc_role: DocumentRole,
}

#[derive(Serialize, Deserialize, sqlx::Type)]
pub enum DocumentRole {
    Viewer,
    Writer,
    Owner,
}

impl Into<String> for DocumentRole {
    fn into(self) -> String {
        match self {
            DocumentRole::Viewer => "viewer",
            DocumentRole::Writer => "writer",
            DocumentRole::Owner => "owner",
        }
        .into()
    }
}

impl From<String> for DocumentRole {
    fn from(s: String) -> Self {
        match s.as_ref() {
            "viewer" => DocumentRole::Viewer,
            "writer" => DocumentRole::Writer,
            _ => panic!("Invalid document role: {s}"),
        }
    }
}
