pub mod authentication;
pub mod documents;

use authentication::*;
use documents::*;

use axum::routing::get;
use axum::routing::post;
use std::error::Error;

use axum::{middleware::from_fn, routing::put, Router};
use dotenv_codegen::dotenv;
use sqlx::postgres::PgPool;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let pool = PgPool::connect(dotenv!("DATABASE_URL")).await?;

    // Set up logging and the way it's formatted.
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::TRACE)
        .without_time()
        .pretty()
        .init();

    let login_router = Router::new()
        .route("/login", post(login))
        .route("/register", post(register));

    let document_router = Router::new()
        .route("/documents", get(get_documents).post(post_document))
        .route(
            "/documents/:document_id",
            get(get_document).put(put_document).delete(delete_document),
        )
        .route("/documents/:document_id/access", get(get_user_access))
        .route(
            "/documents/:document_id/access/:username",
            put(post_user_access).delete(delete_user_access),
        )
        .layer(from_fn(check_token));

    let merged_router = Router::new()
        .merge(login_router)
        .merge(document_router)
        .with_state(pool)
        .layer(tower_http::trace::TraceLayer::new_for_http());

    let listener = tokio::net::TcpListener::bind("127.0.0.1:3000").await?;

    println!("Running REST API...");
    axum::serve(listener, merged_router).await?;

    Ok(())
}
